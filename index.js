const JSON_TEXTAREA = document.getElementById('json-data')
const UPDATE = document.getElementById('update')
const YEAR_INPUT = document.getElementById('year-input')
const WEEKDAYS = document.getElementById('weekdays')
const WEEK = {
  0: 'sunday',
  1: 'monday',
  2: 'tuesday',
  3: 'wednesday',
  4: 'thursday',
  5: 'friday',
  6: 'saturday'
}

const COLORS = ['f72585', 'b5179e', '7209b7', '560bad', '480ca8', '3a0ca3', '3f37c9', '4361ee', '4895ef', '4cc9f0']

UPDATE.addEventListener('click', (e) => {
  e.preventDefault()
  if (Number(YEAR_INPUT.value) > 0) {
    const birthdays = JSON.parse(JSON_TEXTAREA.value)
    const validBirthdays = filterBirthdays(birthdays, Number(YEAR_INPUT.value))
    const mappedInitials = mapInitials(validBirthdays)
    const mappedBirthdays = mapBirthdayToWeekday(mappedInitials, Number(YEAR_INPUT.value))
    createBlocksOfInitials(mappedBirthdays)
  }
})

// Mapping Initials for a person object on the name property
const mapInitials = (persons) => {
  return persons.map(person => {
    person.name = person.name.split(' ')
    let initials = ''
    person.name.forEach(name => {
      initials += name[0]
    })
    person.name = initials
    return person
  })
}

// Filter birthdays according to the given current Input year
const filterBirthdays = (arr, year) => {
  return arr.filter(obj => Number(obj.birthday.split('/')[2]) < year)
}

// Create birthday blocks and append them to a respective weekday
const createBlocksOfInitials = (weekdays) => {
  for (const weekday in weekdays) {
    weekdays[weekday].forEach(person => {
      const block = document.createElement('div')
      const randomColor = generateRandomColors(COLORS, COLORS.length)
      const day = WEEK[weekday]
      const WIDTHPAR = Math.floor(100 / weekdays[weekday].length)
      const HEIGHTPAR = Math.floor(WIDTHPAR * 9 / 10)
      block.setAttribute('class', 'initialsBlock')
      block.innerHTML = person.name
      block.style.backgroundColor = `#${randomColor}`
      block.style.width = `${WIDTHPAR}%`
      block.style.height = `${HEIGHTPAR}%`
      WEEKDAYS.querySelector(`#${day}`).appendChild(block)
    })
  }
}

// Map Birthday to weekday
const mapBirthdayToWeekday = (persons, currYear) => {
  const mapBirthdays = {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
    6: []
  }
  persons.forEach(person => {
    const [day, month] = person.birthday.split('/')
    person.birthday = month + '/' + day + '/' + currYear
    const day_ = new Date(person.birthday)
    console.log(person.birthday)
    mapBirthdays[day_.getDay()].push(person)
  })
  return mapBirthdays
}

// Get Random Colors
function generateRandomColors (colors, max) {
  return colors[Math.floor(Math.random() * max)]
}
